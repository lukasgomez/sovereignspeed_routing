let locations = [];
let countries = [];
let dateInput = '';
let originSelect = '';

function loadScript(url, callback) {
    var script = document.createElement("script");
    script.src = url;
    
    script.onreadystatechange = callback;
    script.onload = callback;

    document.head.appendChild(script);  
}

var mainFunction = function() {   
    originSelect = document.getElementById('origin');
    dateInput = document.getElementById('dateInput');
    const filterInput = document.getElementById('filterInput');
    
    // Blocks:
    let resultsSection = document.getElementById('resultsSection');
    let filterSection = document.getElementById('filterSection');
    let searchBtn = document.getElementById('search-btn');
    let filterBtn = document.getElementById('filter-btn');
    let resultCount = document.getElementById('resultCount');
    
    // Variables:
    let dateSelect = $('#departureDate');
    let routes = [];
    let availableOrigins = [];
    let originOptions = [];
    let filteredRoutes = [];
    let cleanedRoutes = [];
    let selectedOrigin = '';
    let selectedDate = '';
    let originalDestinations = [];

    dateSelect.datepicker({
        calendarWeeks: true,
        todayHighlight: true,
        startDate: new Date(),
        autoclose: true,
        orientation: "auto top",
        daysOfWeekDisabled: [0]
    }).on('changeDate', function(e) {      
        clearResults();

        let stepThree = '<div class="row destination-detail" style="padding-top: 15px;padding-bottom: 15px;padding-right: 15px;padding-left: 15px;margin-right: 15px;margin-left: 15px;border-radius: 13px;border: 1px solid #f9f007 ;">';
        stepThree += '<div class="container">';
        stepThree += '<div class="justify-content-center">';
        stepThree += '<h3 class="text-center" style="margin-bottom: 0;">Now click on the Destination Button to see results.</h3>';
        stepThree += '</div>';
        stepThree += '</div>';
        stepThree += '</div>';

        resultsSection.innerHTML = stepThree; //stepTwo;

        if(e.dates.length != 0) {
            // resultsSection.classList.add('d-none');
        }
    });

    dateSelect.datepicker('update', new Date());

    const getSOVRoutes = new Promise((resolve, reject) => {
        loadSOVRoutes(function(response) {
            // Parse JSON string into object
            resolve(JSON.parse(response));
        });
    });
    
    const getSOVAirports = new Promise((resolve, reject) => {
        loadSOVAirports(function(response) {
            // Parse JSON string into object
            resolve(JSON.parse(response));
        });
    });

    const getCountries = new Promise((resolve, reject) => {
        loadCountriesCode(function(response) {
            // Parse JSON string into object
            resolve(JSON.parse(response));
        });
    });

    getSOVAirports.then((airportsInfo) => {
        locations = [...new Set(airportsInfo.map(airport => { 
            return {
                "codename": airport.Airport, 
                "lat": airport.LAT,
                "lon": airport.LON,
                "name": airport.AirportName,
                "city": airport.CityName,
                "country": airport.Country,
            } 
        }))];
    });

    getSOVRoutes.then((routeCards) => {
        routes = [...new Set(routeCards.map(route => { 
            const originCodeName = route.Routes.substring(0, 3);;
            const destinationCodeName = route.Routes.substring(4, 7);

            const origin = filterValue(locations, 'codename', originCodeName);
            const destination = filterValue(locations, 'codename', destinationCodeName);
            
            return {
                "route": route.Routes, 
                "departure": route.DepartureTime,
                "arrival": route.ArrivalTime,
                "leadTime": route.LeadTime,
                "type": route.Type,
                "monday": route.Monday,
                "tuesday": route.Tuesday,
                "wednesday": route.Wednesday,
                "thursday": route.Thursday,
                "friday": route.Friday,
                "saturday": route.Saturday,
                "sunday": route.Sunday,
                "originCountryName": origin.country,
                "destinationCountryName": destination.country,
            } 
        }))];

        availableOrigins = [...new Set(routeCards.map(route => {
            let origin = route.Routes.substring(0,3); 
            return origin;
        }))];

        let originObjects = [...new Set(locations.filter(airport => {
            if(availableOrigins.includes(airport.codename)) {
                return airport;
            }
        }))];
        
        originObjects.forEach(airport => {
            let option = '<option data-description="' + airport.country + '" value="' + airport.codename + '">' + 
                airport.city + 
                '</option>'; 
            
                originSelect.innerHTML = originSelect.innerHTML + option;
        });

        // Load Tail Select
        loadPicker('#origin');
    });

    getCountries.then((countriesJson) => {
        countries = [...new Set(countriesJson.map(country => {
            return {
                "name": country.name,
                "code": country.code
            }
        }))];
    });
    
    originSelect.addEventListener('change', (event) => {
        let selectedValue = event.target.value;
        selectedOrigin = selectedValue;
        resultsSection.innerHTML = '';

        // UI updates:
        // resultsSection.classList.add('d-none');
        //filterSection.classList.add('d-none');
        cleanedRoutes = [];
        $('#departureDate').datepicker("clearDates");

        let stepTwo = '<div class="row destination-detail" style="padding-top: 15px;padding-bottom: 15px;padding-right: 15px;padding-left: 15px;margin-right: 15px;margin-left: 15px;border-radius: 13px;border: 1px solid #f9f007 ;">';
        stepTwo += '<div class="container">';
        stepTwo += '<div class="justify-content-center">';
        stepTwo += '<h3 class="text-center" style="margin-bottom: 0;">Now select your Departing Date.</h3>';
        stepTwo += '</div>';
        stepTwo += '</div>';
        stepTwo += '</div>';

        resultsSection.innerHTML = stepTwo; //stepTwo;
    });

    searchBtn.addEventListener('click', (event) => {
        resultsSection.innerHTML = '';

        originalDestinations = [...new Set(routes.filter(route => {
            if(route.route.substring(0,3) === selectedOrigin) {
                //resultsSection.innerHTML = resultsSection.innerHTML + drawRouteCard(route);
                return route;
            }
        }))];

        // Clean Routes: 
        let dups = [];
        filteredRoutes = originalDestinations.filter( el => {
            //If is not a duplicate return true; 
            if(dups.indexOf(el.route) == -1) {
                dups.push(el.route);
                return true;
            }

            return false;
        });

        originalDestinations.forEach(route => {
            resultsSection.innerHTML = resultsSection.innerHTML + drawRouteCard(route);
            let destination = getDestinationDetail(route.route.substring(4,7));
            let codeString = route.route+destination.city+destination.country;
            
            cleanedRoutes.push(codeString.toLowerCase().replace(/[ \/]/g, ""));
        });

        if(originalDestinations.length > 0) {
            //console.log('results: ' + originalDestinations.length);
            resultsSection.innerHTML = resultsSection.innerHTML;
            //resultsSection.classList.remove('d-none');
            //filterSection.classList.remove('d-none');
        } else {
            console.log('No results.');
        }

        console.log('Test');
        resultCount.innerHTML = originalDestinations.length;

        let emptyMessage = '<div class="container"><div class="justify-content-center"><h3 class="text-center" style="margin-bottom: 0;">For a new search, please select your Origin.</h3></div></div>';
        resultsSection.innerHTML = resultsSection.innerHTML + emptyMessage;
    });

    filterBtn.addEventListener('click', (event) => {
        filterResults(event);
    });

    filterInput.addEventListener("keyup", function(event) {
        filterResults(event);
    });

    function filterResults(event) {
        event.preventDefault(); 
        let filterString = filterInput.value.toLowerCase();

        let filteredResults = cleanedRoutes.filter(route => {
            if(route.includes(filterString)) {
                let foundRoute = route.substring(0,7);
                return route.substring(0,7);
            }
        });

        let resultsCleaned = filteredResults.unique();
        console.log('filteredResults: ' + resultsCleaned);

        var cardList = document.getElementsByClassName("destination-card");
        resultCount.innerHTML = filteredResults.length;
        
        Array.from(cardList).forEach(function (element) { 
            let currentId = element.id;
            let cleanCurrentId = currentId.toLowerCase().replace(/[0-9]/g, "")

            if(resultsCleaned.includes(cleanCurrentId)) {
                fadeIn(element);
            } else {
                fadeOut(element);
            }
        }); 
    }
}

loadScript('js/functions.js');
loadScript('js/promises.js', mainFunction);