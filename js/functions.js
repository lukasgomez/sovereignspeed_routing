Array.prototype.contains = function(v) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === v) return true;
    }
    return false;
};
  
Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        if (!arr.contains(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr;
}

function filterValue(obj, key, value) {
    return obj.find(function(v){ return v[key] === value});
}

function clearResults() {
    resultsSection.innerHTML = ''; 
    originalDestinations = [];
    resultCount.innerHTML = originalDestinations.length;
}

// ** FADE OUT FUNCTION **
function fadeOut(el) {
    el.style.opacity = 1;
    (function fade() {
        if ((el.style.opacity -= .1) < 0) {
            el.style.display = "none";
        } else {
            requestAnimationFrame(fade);
        }
    })();
};

// ** FADE IN FUNCTION **
function fadeIn(el, display) {
    el.style.opacity = 0;
    el.style.display = display || "";
    (function fade() {
        var val = parseFloat(el.style.opacity);
        if (!((val += .1) >= 1)) {
            el.style.opacity = val;
            requestAnimationFrame(fade);
        }
    })();
};

function getFormatterCurrentDate() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    
    today = mm + '/' + dd + '/' + yyyy;

    return today;
}

function getWeek(fromDate){
    let sunday = new Date(fromDate.setDate(fromDate.getDate()-fromDate.getDay()));
    let result = [new Date(sunday)];
    
    while (sunday.setDate(sunday.getDate()+1) && sunday.getDay()!==0) {
        result.push(new Date(sunday));
    }

    return result;
}

function loadPicker(elementName) {
    tail.select(elementName, {
        animate: true,              // [0.3.0]      Boolean
        classNames: null,           // [0.2.0]      Boolean, String, Array, null
        csvOutput: false,           // [0.3.4]      Boolean
        csvSeparator: ",",          // [0.3.4]      String
        descriptions: true,         // [0.3.0]      Boolean
        deselect: true,            // [0.3.0]      Boolean
        disabled: false,            // [0.5.0]      Boolean
        height: 350,                // [0.2.0]      Integer, null
        hideDisabled: false,        // [0.3.0]      Boolean
        hideSelected: false,        // [0.3.0]      Boolean
        //items: originOptions,        // [0.3.0]      Object
        locale: "en",               // [0.5.0]      String
        linguisticRules: {          // [0.5.9]      Object
            "е": "ё",
            "a": "ä",
            "o": "ö",
            "u": "ü",
            "ss": "ß"
        },
        multiple: false,            // [0.2.0]      Boolean
        multiLimit: Infinity,       // [0.3.0]      Integer, Infinity
        multiPinSelected: false,    // [0.5.0]      Boolean
        multiContainer: false,      // [0.3.0]      Boolean, String
        multiShowCount: true,       // [0.3.0]      Boolean
        multiShowLimit: false,      // [0.5.0]      Boolean
        multiSelectAll: false,      // [0.4.0]      Boolean
        multiSelectGroup: true,     // [0.4.0]      Boolean
        openAbove: null,            // [0.3.0]      Boolean, null
        placeholder: "Pick an Origin...", // [0.2.0]      String, null
        search: true,              // [0.3.0]      Boolean
        searchConfig: [             // [0.5.13]     Array
            "text", "value", "attributes"
        ],
        searchDisabled: true,       // [0.5.5]      Boolean
        searchFocus: true,          // [0.3.0]      Boolean
        searchMarked: true,         // [0.3.0]      Boolean
        searchMinLength: 1,         // [0.5.13]     Integer
        sortItems: false,           // [0.3.0]      String, Function, false
        sortGroups: false,          // [0.3.0]      String, Function, false
        sourceBind: false,          // [0.5.0]      Boolean
        sourceHide: true,           // [0.5.0]      Boolean
        startOpen: false,           // [0.3.0]      Boolean
        stayOpen: false,            // [0.3.0]      Boolean
        width: '100%',              // [0.2.0]      Integer, String, null
        cbComplete: undefined,      // [0.5.0]      Function
        cbEmpty: undefined,         // [0.5.0]      Function
        cbLoopItem: undefined,      // [0.4.0]      Function
        cbLoopGroup: undefined      // [0.4.0]      Function
    });
}

function getCountryCode(countryName) {
    let countryCode = countries.find(country => {        
        if(countryName === country.name) {
            return country.code;
        }
    });

    return countryCode ? countryCode : { "code": "doesnt-exist" };
}

function getDestinationDetail(cityCode) {
    let cityName = locations.find(airport => {        
        if(cityCode === airport.codename) {
            return airport;
        }
    });

    return cityName;
}

function drawRouteCard(route) {
    let destination = getDestinationDetail(route.route.substring(4,7));
    let countryCode = getCountryCode(destination.country).code.toLowerCase();
    let serviceEmail = 'cs.' + route.route.substring(0,3) + '@sovereignspeed.com';

    let workingDays = '';
    workingDays += route.monday ? 'M' : '';
    workingDays += route.tuesday ? 'T' : '';
    workingDays += route.wednesday ? 'W' : '';
    workingDays += route.thursday ? 'T' : '';
    workingDays += route.friday ? 'F' : '';
    workingDays += route.saturday ? 'S' : '';
    workingDays += route.sunday ? 'S' : '';
    workingDays = '(' + workingDays.split('').join('•') + ')';

    switch(route.leadTime.charAt(0)) {
        case '1':
            timeLapsColor = '#3cb043';
            break;
        case '2':
            timeLapsColor = '#dcdcdc';
            break;
        case '3':
            timeLapsColor = '#ff8400';
            break;
    }

    // New Card: 
    let htmlCard = '<div id="' + route.route.toLowerCase() + destination.city.replace(/[ \/]/g, "") + destination.country.replace(/[ \/]/g, "") + route.departure.replace(":", "") + route.arrival.replace(":", "") + '" class="row destination-detail destination-card" style="padding-top: 15px;padding-bottom: 15px;padding-right: 15px;padding-left: 15px;margin-right: 15px;margin-left: 15px;border-radius: 13px;border: 1px solid #f9f007 ;">';
    htmlCard += '<div class="col-12 col-sm-6 col-md-6 col-lg-1 col-xl-1 d-flex d-sm-flex flex-row justify-content-center align-items-center justify-content-sm-center align-items-sm-center flex-lg-column flex-xl-column justify-content-xl-center align-items-xl-center destination-time"><span class="flag flag-' + countryCode + '"></span>';
    htmlCard += '<h6 class="d-sm-flex align-items-sm-center timelaps" style="color:' + timeLapsColor + '">&nbsp;' + route.leadTime + '</h6>';
    htmlCard += '</div>';
    htmlCard += '<div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 d-flex d-xl-flex flex-column justify-content-center align-items-center justify-content-xl-end align-items-xl-center destination-detail" style="padding-top: 15px;padding-bottom: 15px;">';
    htmlCard += '<h5 class="d-sm-flex justify-content-sm-center align-items-sm-center">Destination:</h5>';
    htmlCard += '<h2 class="d-sm-flex justify-content-sm-center align-items-sm-center">' + destination.city + '</h2>';
    htmlCard += '<h6 class="d-sm-flex justify-content-sm-center">Route: ' + route.route + '</h6>';
    htmlCard += '</div>';
    htmlCard += '<div class="col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2 d-flex d-xl-flex flex-column justify-content-center justify-content-xl-center align-items-xl-center destination-departure-time" style="padding-top: 15px;padding-bottom: 15px;">';
    htmlCard += '<p class="text-center">Departure Time</p>';
    htmlCard += '<h4 class="text-center">' + route.departure + '</h4>';
    htmlCard += '<p class="text-center">' + workingDays + '</p>';
    htmlCard += '</div>';
    htmlCard += '<div class="col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2 d-flex d-xl-flex flex-column justify-content-center justify-content-xl-center align-items-xl-center destination-arrival-time" style="padding-top: 15px;padding-bottom: 15px;">';
    htmlCard += '<p class="text-center">Arrival Time</p>';
    htmlCard += '<h4 class="text-center">' + route.arrival + '</h4>';
    htmlCard += '<p class="text-center">•</p>';
    htmlCard += '</div>';
    htmlCard += '<div class="col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2 d-flex d-sm-flex d-xl-flex flex-column justify-content-center align-items-center justify-content-sm-center align-items-sm-center justify-content-xl-center align-items-xl-center destination-transport" style="padding-top: 15px;padding-bottom: 15px;">';
    
    if(route.type.includes('S')) {
        htmlCard += '<img class="img-fluid" src="img/sov_sprinter_icon.png?h=0de50f4b9ba8939d4c7d717fa1fe3c29" style="margin-bottom: 15px;">';
    }

    if(route.type.includes('T')) {
        htmlCard += '<img class="img-fluid" src="img/sov_truck_icon.png?h=0de50f4b9ba8939d4c7d717fa1fe3c29">';
    }

    htmlCard += '</div>';
    htmlCard += '<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 text-center d-lg-flex d-xl-flex justify-content-center justify-content-lg-center justify-content-xl-center align-items-xl-center destination-book-now" style="padding-top: 15px;padding-bottom: 15px;">';
    htmlCard += '<a class="btn btn-default bookBtn" onclick="prepareEmail(\'' + serviceEmail + '\',\'' + route.route + '\',\'' + route.originCountryName + '\',\'' + route.destinationCountryName + '\',\'' + route.departure + '\',\'' + route.arrival + '\')">Book Now</button>';
    htmlCard += '</div>';
    htmlCard += '</div>';

    return htmlCard;
}

function prepareEmail(serviceEmail, route, originCountryName, destinationCountryName, departure, arrival) {    
    // Set the hreflink
    var mailDetail = 'mailto:' + serviceEmail;
    mailDetail += '?subject=' + route + '%20|%20' + dateInput.value; // Ruta + Fecha
    mailDetail += '&body=Booking%20Details%3A%0D%0A';
    mailDetail += 'Origin%3A%20' + originCountryName + '%0D%0A';
    mailDetail += 'Destination%3A%20' + destinationCountryName + '%0D%0A';
    mailDetail += 'Collect%20Time%3A%20' + departure + '%0D%0A';
    mailDetail += 'Deliver%20Time%3A%20' + arrival + '%0D%0A';
    mailDetail += 'Fill%20in%20the%20weight%20information%20of%20your%20shipment%3A%20%0D%0A';
    mailDetail += 'Fill%20in%20dimensions%20of%20your%20shipment%3A%20%0D%0A';

    if(originCountryName === 'United Kingdom' || 
        originCountryName === 'Ireland' || 
        destinationCountryName === 'United Kingdom' || 
        destinationCountryName === 'Ireland') {
        mailDetail += '%0D%0A%0D%0A' + '** Please attach T documents and specify these T documents.';
        mailDetail += '%0D%0A' + 'Note: Due to BREXIT all departures and destinations in UK and IRELAND have an extra 24 hours lead time due to customs clearance';
    }
    // set ireland notes

    //Create link
    var a = document.createElement('a');
    a.href = mailDetail;
    a.click();
}